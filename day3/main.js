const fs = require('fs');

let input = fs.readFileSync("input.txt").toString().split("\n");

const rucksacks = input.map((line) => {
    const half = line.length / 2;
    const compartment1 = line.slice(0, half);
    const compartment2 = line.slice(half);
    const inBoth = compartment1.split('').filter((char) => compartment2.includes(char))[0];
    const isUpper = inBoth === inBoth.toUpperCase();
    const priority = isUpper ? inBoth.charCodeAt(0) - 38 : inBoth.charCodeAt(0) - 96;

    return {
        compartment1,
        compartment2,
        inBoth,
        priority
    }
});

const prioritySum = rucksacks.reduce((sum, rucksack) => sum + rucksack.priority, 0);
console.log(prioritySum);

const groups = [];
for (let i = 0; i < input.length; i += 3) {
    const group = input.slice(i, i + 3);

    const rucksack1 = input[i];
    const rucksack2 = input[i + 1];
    const rucksack3 = input[i + 2];

    const inAll = rucksack1.split('').filter((char) => rucksack2.includes(char) && rucksack3.includes(char))[0];

    const isUpper = inAll === inAll.toUpperCase();

    const priority = isUpper ? inAll.charCodeAt(0) - 38 : inAll.charCodeAt(0) - 96;

    groups.push({
        group,
        priority
    });
}

const prioritySum2 = groups.reduce((sum, group) => sum + group.priority, 0);
console.log(prioritySum2);