const fs = require("fs");

let input = fs.readFileSync("input.txt").toString().trimEnd();

function part1(input) {
    let count = 0;
    for (const line of input.split('\n')) {
      const [[a1, b1], [a2, b2]] = line
        .split(',')
        .map((elf) => elf.split('-').map(Number));
      if ((a1 <= a2 && b1 >= b2) || (a1 >= a2 && b1 <= b2)) {
        count++;
      }
    }
    console.log(count);
}

function part2(input) {
    let count = 0;
    for (const line of input.split('\n')) {
      const [[a1, b1], [a2, b2]] = line
        .split(',')
        .map((elf) => elf.split('-').map(Number));
      if ((a1 >= a2 && a1 <= b2) || (a2 >= a1 && a2 <= b1)) {
        count++;
      }
    }
    console.log(count);
}
part1(input);
part2(input);