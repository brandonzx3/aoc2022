const fs = require("fs");
let input = fs.readFileSync("input.txt").toString().split("\n");

//input
//a = rock
//b = paper
//c = scissors

//response
//x = rock
//y = paper
//z = scissors


function part1() {
    let sum = 0;
    for(var i = 0; i < input.length; i++) {
        let opponent = input[i].split(" ")[0];
        let response = input[i].split(" ")[1];
        sum += round(opponent, response);
    }
    return sum;
}

function part2() {
    var sum = 0;
    for(var i = 0; i < input.length; i++) {
        let opponent = input[i].split(" ")[0];
        let indicator = input[i].split(" ")[1];
        var response;

        if(indicator == "X") {
            //need to loose
            if(opponent == "A") response = "Z";
            if(opponent == "B") response = "X";
            if(opponent == "C") response = "Y";
        }

        if(indicator == "Y") {
            //need to draw
            if(opponent == "A") response = "X";
            if(opponent == "B") response = "Y";
            if(opponent == "C") response = "Z";
        }

        if(indicator == "Z") {
            //need to win
            if(opponent == "A") response = "Y";
            if(opponent == "B") response = "Z";
            if(opponent == "C") response = "X";
        }

        sum += round(opponent, response);
    }
    return sum;
}

function round(opponent, response) {
    let sum = 0;
    if((opponent == "A" && response == "X") || (opponent == "B" && response == "Y") || (opponent == "C" && response == "Z")) {
        //draw
        sum += 3;
    }

    if((opponent == "A" && response == "Y") || (opponent == "B" && response == "Z") || (opponent == "C" && response == "X")) {
        //win
        sum += 6;
    }

    if((opponent == "A" && response == "Z") || (opponent == "B" && response == "X") || (opponent == "C" && response == "B")) {
        //loose
        sum += 0;
    }
    if(response == "X") sum += 1;
    if(response == "Y") sum += 2;
    if(response == "Z") sum += 3;
    return sum;
}

console.log(part1());
console.log(part2());