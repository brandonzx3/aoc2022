const fs = require("fs");

let input = fs.readFileSync("input.txt").toString().split(/\n\s*\n/);

function part1() {
    let most = 0;
    for(var i = 0; i < input.length; i++) {
        var sum = 0;
        sum += calories(input[i]);
        if(sum > most) most = sum;
    }
    return most;
}

function part2() {
    let sums = [];
    for(var i = 0; i < input.length; i++) {
        sums.push(calories(input[i]));
    }
    sums.sort(function(a, b) {
      return a - b;
    });    
    sums.reverse();
    return sums[0] + sums[1] + sums[2];
}   

function calories(input) {
    var nums = input.toString().split("\n");
    var sum = 0;
    for(var x = 0; x < nums.length; x++) {
        sum += parseInt(nums[x]);
    }
    return sum;
}

console.log(part1());
console.log(part2());